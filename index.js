module.exports = (function () {
    var express = require('express');
    var app = express();
    
    var client = require('./client');
    var server = require('./server');
    var config = require('./config.json');

    app.use('/api', server);
    app.use('/', client);
    
    app.listen(config.server.port, function(){
        var prefix = config.server.ssl ? 'https://' : 'http://';
        var webUrl = prefix + config.server.host  + ':' + config.server.port + '/';
        var apiUrl = webUrl + 'api/';
        var explorerUrl = apiUrl + 'explorer/';
        console.log('web server started at ' + webUrl);
        console.log('api server started at ' + apiUrl);
        console.log('docs server started at ' + explorerUrl);
    })

    return app;
})();

